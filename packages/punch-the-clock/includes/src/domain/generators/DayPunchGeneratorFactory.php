<?php

namespace punchTheClock\domain\generators;

/**
 * Class DayPunchGeneratorFactory
 *
 * @package punchTheClock\domain\generators
 */
class DayPunchGeneratorFactory
{
    /**
     * @var PunchGeneratorFactory
     */
    private PunchGeneratorFactory $punchGeneratorFactory;

    /**
     * DayPunchGeneratorFactory constructor.
     *
     * @param PunchGeneratorFactory $punchGeneratorFactory
     */
    public function __construct( PunchGeneratorFactory $punchGeneratorFactory )
    {
        $this->punchGeneratorFactory = $punchGeneratorFactory;
    }

    public function create(): array
    {
        return [
            $this->punchGeneratorFactory->create( 9 ),
            $this->punchGeneratorFactory->create( 12, -3, 3 ),
            $this->punchGeneratorFactory->create( 13, -9, 9 ),
            $this->punchGeneratorFactory->create( 18, -5, 5 ),
        ];
    }
}
