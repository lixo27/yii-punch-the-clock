<?php

namespace punchTheClock\domain\generators;

/**
 * Class PunchGeneratorFactory
 *
 * @package punchTheClock\domain\generators
 */
class PunchGeneratorFactory
{
    public function create( $hour, $minTolerance = -7, $maxTolerance = 7 ): \DateTime
    {
        $tolerance = rand( $minTolerance, $maxTolerance );

        $punch = new \DateTime();
        $punch->setTime( $hour, 0, 0 );
        $punch->modify( "{$tolerance} minutes" );

        return $punch;
    }
}
