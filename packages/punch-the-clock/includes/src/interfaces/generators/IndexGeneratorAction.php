<?php

namespace punchTheClock\interfaces\generators;

use punchTheClock\application\generators\query\GetMonthGeneratorQuery;
use punchTheClock\application\generators\query\GetWeekGeneratorQuery;
use yii\base\Action;

/**
 * Class IndexGeneratorAction
 *
 * @package punchTheClock\interfaces\generators
 */
class IndexGeneratorAction extends Action
{
    /**
     * @var GetWeekGeneratorQuery
     */
    private GetWeekGeneratorQuery $weekGeneratorQuery;

    /**
     * IndexGeneratorAction constructor.
     *
     * @param                       $id
     * @param                       $controller
     * @param GetWeekGeneratorQuery $weekGeneratorQuery
     * @param array                 $config
     */
    public function __construct( $id, $controller, GetWeekGeneratorQuery $weekGeneratorQuery, $config = [] )
    {
        parent::__construct( $id, $controller, $config );
        $this->weekGeneratorQuery = $weekGeneratorQuery;
    }

    public function run()
    {
        return $this->controller->render( 'index', [
            'week' => $this->weekGeneratorQuery->execute(),
        ] );
    }
}
