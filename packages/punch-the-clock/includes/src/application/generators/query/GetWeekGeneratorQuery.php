<?php

namespace punchTheClock\application\generators\query;

use punchTheClock\domain\generators\DayPunchGeneratorFactory;

/**
 * Class GetWeekGeneratorQuery
 *
 * @package punchTheClock\application\generators\query
 */
class GetWeekGeneratorQuery
{
    /**
     * @var DayPunchGeneratorFactory
     */
    private DayPunchGeneratorFactory $dayPunchGeneratorFactory;

    /**
     * GetWeekGeneratorQuery constructor.
     *
     * @param DayPunchGeneratorFactory $dayPunchGeneratorFactory
     */
    public function __construct( DayPunchGeneratorFactory $dayPunchGeneratorFactory )
    {
        $this->dayPunchGeneratorFactory = $dayPunchGeneratorFactory;
    }

    public function execute(): array
    {
        return [
            $this->dayPunchGeneratorFactory->create(),
            $this->dayPunchGeneratorFactory->create(),
            $this->dayPunchGeneratorFactory->create(),
            $this->dayPunchGeneratorFactory->create(),
            $this->dayPunchGeneratorFactory->create()
        ];
    }
}
