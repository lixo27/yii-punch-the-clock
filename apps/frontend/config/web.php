<?php

return call_user_func( function () {
    return [
        'id' => 'frontend',
        'basePath' => dirname( __DIR__ ),
        'aliases' => [
            '@bower' => '@vendor/bower-asset',
            '@npm' => '@vendor/npm-asset',
        ],
        'components' => [
            'request' => [
                'cookieValidationKey' => '7UrgpftbTX4_a9IavRx06V8GdbRfAxMw',
            ],
        ],
    ];
} );
