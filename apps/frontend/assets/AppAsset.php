<?php

namespace app\assets;

use yii\helpers\Url;
use yii\web\AssetBundle;

/**
 * Class AppAsset
 *
 * @package app\assets
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';

    public $baseUrl = '@web';

    public $css = [
        'css/site.css',
    ];

    public $js = [
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public function registerAssetFiles( $view )
    {
        parent::registerAssetFiles( $view );

        $view->registerLinkTag( [
            'rel' => 'icon',
            'href' => Url::base() . '/favicon.ico'
        ] );
    }
}
