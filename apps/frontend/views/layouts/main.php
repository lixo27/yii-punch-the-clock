<?php /** @var \yii\web\View $this */ ?>
<?php /** @var string $content */ ?>
<?php \app\assets\AppAsset::register( $this ); ?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>

    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <?php $this->registerCsrfMetaTags() ?>

    <title><?= \yii\helpers\Html::encode( $this->title ) ?></title>
    <?php $this->head() ?>

</head>
<body>

<?php $this->beginBody() ?>

<div class="wrap">

    <div class="container">

        <?= $content ?>

    </div>

</div>

<?php $this->endBody() ?>

</body>
</html>

<?php $this->endPage() ?>
