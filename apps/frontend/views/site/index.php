<?php /** @var \yii\web\View $this */ ?>
<?php /** @var DateTime[] $week */ ?>

<?php $this->title = 'Punch the Clock!'; ?>

<div class="jumbotron">

    <h1>Punch the Clock!</h1>

    <p class="lead">Mauris pharetra eros accumsan risus lacinia pretium a eget nulla. Phasellus non vulputate neque.</p>

</div><!-- .jumbotron -->

<div class="row">

    <div class="col-lg-6 col-lg-push-3">

        <table class="table table-bordered">

            <tbody>

            <?php foreach ( $week as $weekDay ): ?>

                <tr>
                    <td><?= $weekDay[ 0 ]->format( 'H:i' ); ?></td>
                    <td><?= $weekDay[ 1 ]->format( 'H:i' ); ?></td>
                    <td><?= $weekDay[ 2 ]->format( 'H:i' ); ?></td>
                    <td><?= $weekDay[ 3 ]->format( 'H:i' ); ?></td>
                </tr>

            <?php endforeach; ?>

            </tbody>

        </table><!-- .table -->

    </div><!-- .col-lg-12 -->

</div><!-- .row -->
